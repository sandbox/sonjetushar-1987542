<?php
/**
 * AvaAddressType.class.php
 */

/**
 * The type of the address(es) returned in the validation result.
 *
 * @author    Avalara
 * @copyright 2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AddressSvc
 *
 */
namespace Avalara\AddressSvc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaAddressType extends AvaEnum
    {
        public static $FirmOrCompany 	= 'F';
        public static $GeneralDelivery 	= 'G';
        public static $HighRise         = 'H';
        public static $POBox            = 'P';
        public static $RuralRoute       = 'R';
        public static $StreetOrResidential = 'S';

        public static function Values()
        {
            return array(
                'FirmOrCompany'         => AvaAddressType::$FirmOrCompany,
                'GeneralDelivery'       => AvaAddressType::$GeneralDelivery,
                'HighRise'              => AvaAddressType::$HighRise,
                'POBox'                 => AvaAddressType::$POBox,
                'RuralRoute'            => AvaAddressType::$RuralRoute,
                'StreetOrResidential'   => AvaAddressType::$StreetOrResidential
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }
    }

}
