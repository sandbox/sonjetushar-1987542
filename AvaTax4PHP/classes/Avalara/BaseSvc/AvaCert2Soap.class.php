<?php

/**
 * AvaCert2Soap class
 * 
 *  
 * 
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */
use Avalara\AvaCert2Svc\AvaCustomerSaveRequest as AvaCustomerSaveRequest;
use Avalara\AvaCert2Svc\AvaCertificateRequestInitiateRequest as AvaCertificateRequestInitiateRequest;
use Avalara\AvaCert2Svc\AvaCertificateGetRequest as AvaCertificateGetRequest;
use Avalara\AvaCert2Svc\AvaCertificateRequestGetRequest as AvaCertificateRequestGetRequest;
use Avalara\AvaCert2Svc\AvaCertificateImageGetRequest as AvaCertificateImageGetRequest;
class AvaCert2Soap extends AvaAvalaraSoapClient {

  static $servicePath = '/AvaCert2/AvaCert2Svc.asmx';	
  private static $classmap = array(
                                    'CustomerSave' => '\Avalara\AvaCert2Svc\CustomerSave',
                                    'CustomerSaveRequest' => '\Avalara\AvaCert2Svc\AvaCustomerSaveRequest',
                                    'Customer' => '\Avalara\AvaCert2Svc\AvaCustomer',
                                    'Certificate' => '\Avalara\AvaCert2Svc\AvaCertificate',
                                    'CertificateStatus' => '\Avalara\AvaCert2Svc\AvaCertificateStatus',
                                    'ReviewStatus' => '\Avalara\AvaCert2Svc\AvaReviewStatus',
                                    'CertificateUsage' => '\Avalara\AvaCert2Svc\AvaCertificateUsage',
                                    'CertificateJurisdiction' => '\Avalara\AvaCert2Svc\AvaCertificateJurisdiction',
                                    'CustomerSaveResult' => '\Avalara\AvaCert2Svc\AvaCustomerSaveResult',
                                    'Profile' => 'AvaProfile',
                                    'CertificateRequestInitiate' => '\Avalara\AvaCert2Svc\CertificateRequestInitiate',
                                    'CertificateRequestInitiateRequest' => '\Avalara\AvaCert2Svc\AvaCertificateRequestInitiateRequest',
                                    'CertificateRequestInitiateResult' => '\Avalara\AvaCert2Svc\AvaCertificateRequestInitiateResult',
                                    'CertificateGet' => '\Avalara\AvaCert2Svc\CertificateGet',
                                    'CertificateGetRequest' => '\Avalara\AvaCert2Svc\AvaCertificateGetRequest',
                                    'CommunicationMode' => '\Avalara\AvaCert2Svc\AvaCommunicationMode',
                                    'CertificateGetResult' => '\Avalara\AvaCert2Svc\AvaCertificateGetResult',
                                    'CertificateRequestGet' => '\Avalara\AvaCert2Svc\CertificateRequestGet',
                                    'CertificateRequestGetRequest' => '\Avalara\AvaCert2Svc\AvaCertificateRequestGetRequest',
                                    'CertificateRequestGetResult' => '\Avalara\AvaCert2Svc\AvaCertificateRequestGetResult',
                                    'CertificateRequest' => '\Avalara\AvaCert2Svc\AvaCertificateRequest',
                                    'CertificateRequestStatus' => '\Avalara\AvaCert2Svc\AvaCertificateRequestStatus',
                                    'CertificateRequestStage' => '\Avalara\AvaCert2Svc\AvaCertificateRequestStage',
                                    'CertificateImageGet' => '\Avalara\AvaCert2Svc\CertificateImageGet',
                                    'CertificateImageGetRequest' => '\Avalara\AvaCert2Svc\AvaCertificateImageGetRequest',
                                    'FormatType' => '\Avalara\AvaCert2Svc\AvaFormatType',
                                    'CertificateImageGetResult' => '\Avalara\AvaCert2Svc\AvaCertificateImageGetResult',
                                    'BaseRequest' => '\Avalara\BaseSvc\BaseRequest',
                                    'RequestType' => '\Avalara\AvaCert2Svc\AvaRequestType',
                                    'BaseResult' => '\Avalara\BaseSvc\AvaBaseResult',
                                    'SeverityLevel' => '\Avalara\BaseSvc\AvaSeverityLevel',
                                    'Message' => '\Avalara\BaseSvc\AvaMessage',
                                    'Ping' => 'AvaPing',
                                    'PingResult' => '\Avalara\BaseSvc\AvaPingResult',
                                    'IsAuthorized' => 'AvaIsAuthorized',
                                    'IsAuthorizedResult' => '\Avalara\BaseSvc\AvaIsAuthorizedResult',
                                   );

public function __construct($configurationName = 'Default')
    {
        $config = new AvaATConfig($configurationName);
        
        $this->client = new AvaDynamicSoapClient   (
            $config->avacert2WSDL,
            array
            (
                'location' => $config->url.$config->avacert2Service, 
                'trace' => $config->trace,
                'classmap' => AvaCert2Soap::$classmap
            ), 
            $config
        );
    }

  /**
   * This method adds an exempt customer record to AvaCert.
   *
   * <pre>
   * use \Avalara\AvaCert2Svc\AvaCustomer as AvaCustomer;
   * use \Avalara\AvaCert2Svc\AvaCustomerSaveRequest as AvaCustomerSaveRequest;
   * $customer = new AvaCustomer();
   * $customer->setCompanyCode("DEFAULT");
   * $customer->setCustomerCode("AVALARA");
   * $customer->setBusinessName("Avalara, Inc.");
   * $customer->setAddress1("435 Ericksen Ave NE");
   * $customer->setCity("Bainbridge Island");
   * $customer->setState("WA");
   * $customer->setZip("98110");
   * $customer->setCountry("US");
   * $customer->setEmail("info@avalara.com");
   * $customer->setPhone("206-826-4900");
   * $customer->setFax("206-780-5011");
   * $customer->setType("Bill_To");
   *
   * $customerSaveRequest = new AvaCustomerSaveRequest();
   * $customerSaveRequest->setCustomer($customer);
   * 
   * $customerSaveResult= $avacert2Service->customerSave($customerSaveRequest);
   * </pre> 
   *
   * @param CustomerSave $parameters
   * @return AvaCustomerSaveResult
   */

  public function CustomerSave(AvaCustomerSaveRequest $customerSaveRequest) {
      return $this->client->CustomerSave(array('CustomerSaveRequest' => $customerSaveRequest))->CustomerSaveResult;
  }

  /**
   * This method initiates a request from AvaCert to the customer for an exemption certificate.
   * The request will be sent using the designated method (email, fax, post).
   *
   * <pre>
   * $certificateRequestInitiateRequest=new AvaCertificateRequestInitiateRequest();
   * $certificateRequestInitiateRequest->setCompanyCode("DEFAULT");
   * $certificateRequestInitiateRequest->setCustomerCode("AVALARA");
   * $certificateRequestInitiateRequest->setCommunicationMode(AvaCommunicationMode::$EMAIL);
   * $certificateRequestInitiateRequest->setCustomMessage("Thank you!");
   *
   * $certificateRequestInitiateResult= $avacert2Service->certificateRequestInitiate($certificateRequestInitiateRequest); 
   * </pre>
   * 
   * @param CertificateRequestInitiate $parameters
   * @return AvaCertificateRequestInitiateResult
   */
  public function CertificateRequestInitiate(AvaCertificateRequestInitiateRequest $certificateRequestInitiateRequest) {
      return $this->client->CertificateRequestInitiate(array('CertificateRequestInitiateRequest' => $certificateRequestInitiateRequest))->CertificateRequestInitiateResult;
  }
  
  /**
   * This method retrieves all certificates from vCert for a particular customer. 
   * 
   * <pre>
   * $certificateGetRequest=new AvaCertificateGetRequest();
   * $certificateGetRequest->setCompanyCode("DEFAULT");
   * $certificateGetRequest->setCustomerCode("AVALARA");
   *
   * $certificateGetResult= $avacert2Service->certificateGet($certificateGetRequest); 
   * </pre>
   * 
   * @param CertificateGet $parameters
   * @return AvaCertificateGetResult
   */
  public function CertificateGet(AvaCertificateGetRequest $certificateGetRequest) {
    return $this->client->CertificateGet(array('CertificateGetRequest' => $certificateGetRequest))->CertificateGetResult;
  }


  /**
   * This method retrieves all certificate requests from vCert for a particular customer. 
   * 
   * <pre>
   * $certificateRequestGetRequest=new AvaCertificateRequestGetRequest();
   * $certificateRequestGetRequest->setCompanyCode("DEFAULT");
   * $certificateRequestGetRequest->setCustomerCode("AVALARA");
   * $certificateRequestGetRequest->setRequestStatus(AvaCertificateRequestStatus::$OPEN);
   *
   * $certificateRequestGetResult= $avacert2Service->certificateRequestGet($certificateRequestGetRequest); 
   * </pre> 
   *
   * @param CertificateRequestGet $parameters
   * @return AvaCertificateRequestGetResult
   */
  public function CertificateRequestGet(AvaCertificateRequestGetRequest $certificateRequestGetRequest) {
  	return $this->client->CertificateRequestGet(array('CertificateRequestGetRequest' => $certificateRequestGetRequest))->CertificateRequestGetResult;
  }

  /**
   * This method retrieves all certificate requests from vCert for a particular customer. 
   * 
   * <pre>
   * $certificateImageGetRequest=new AvaCertificateImageGetRequest();
   * $certificateImageGetRequest->setCompanyCode("DEFAULT");
   * $certificateImageGetRequest->setAvaCertId("CBSK");
   * $certificateImageGetRequest->setFormat(AvaFormatType::$PNG);
   * $certificateImageGetRequest->setPageNumber(1);
   *
   * $certificateImageGetResult= $avacert2Service->certificateImageGet($certificateImageGetRequest); 
   * </pre>  
   *
   * @param CertificateImageGet $parameters
   * @return AvaCertificateImageGetResult
   */
  public function CertificateImageGet(AvaCertificateImageGetRequest $certificateImageGetRequest) {
  	return $this->client->CertificateImageGet(array('CertificateImageGetRequest' => $certificateImageGetRequest))->CertificateImageGetResult;
  }
  
  /**
   * Verifies connectivity to the web service and returns version information about the service. 
   *
   * @param AvaPing $parameters
   * @return AvaPingResult
   */
  public function Ping($message = '') {
    return $this->client->Ping(array('Message' => $message))->PingResult;
  }

  /**
   * Checks authentication of and authorization to one or more operations on the service.
   * <p>
   * This operation allows pre-authorization checking of any or all operations.
   * It will return a comma delimited set of operation names which will be all or a subset
   * of the requested operation names.  For security, it will never return operation names
   * other than those requested, i.e. protects against phishing.
   * </p>
   * <b>Example:</b><br>
   * <code> isAuthorized("CustomerSave,CertificateRequestInitiate")</code>
   * @param AvaIsAuthorized $parameters
   * @return AvaIsAuthorizedResult
   */
  public function IsAuthorized(IsAuthorized $parameters) {
    return $this->client->IsAuthorized(array('Operations' => $operations))->IsAuthorizedResult;
  }

}