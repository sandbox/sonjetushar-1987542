<?php
/**
 * AvaAvalaraSoapClientent.class.php
 */

/**
 * Abstract base class for all Avalara web service clients.
 *
 * Users should never create instances of this class.
 *
 * @abstract
 * @see AddressServiceSoap
 * @see AvaTaxServiceSoap
 * 
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */

class AvaAvalaraSoapClient
{
    protected $client;

    public function __getLastRequest() { return $this->client->__getLastRequest(); }
    public function __getLastResponse() { return $this->client->__getLastResponse(); }
    public function __getLastRequestHeaders() { return $this->client->__getLastRequestHeaders(); }
    public function __getLastResponseHeaders() { return $this->client->__getLastResponseHeaders(); }

}