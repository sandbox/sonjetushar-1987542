<?php
/**
 * AvaATConfig.class.php
 */

/**
 * Contains various service configuration parameters as class static variables.
 *
 * {@link AvaAddressServiceSoap} and {@link AvaTaxServiceSoap} read this file during initialization.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */

class AvaATConfig
{
    private static $Configurations = array();
    private $_ivars;

    public function __construct($name, $values = null)
    {
        if ($values) {
            AvaATConfig::$Configurations[$name] = $values;
        }
        $this->_ivars = AvaATConfig::$Configurations[$name];
    }

    public function __get($n)
    {
        if ($n == '_ivars') {
            return parent::__get($n);
        }
        if (isset($this->_ivars[$n])) {
            return $this->_ivars[$n];
        } else if (isset(AvaATConfig::$Configurations['Default'][$n])) // read missing values from default
        {
            return AvaATConfig::$Configurations['Default'][$n];
        } else {
            return null;
        }
    }
}

/* Specify configurations by name here.  You can specify as many as you like */


$__wsdldir = dirname(__FILE__) . "/wsdl";

/* This is the default configuration - it is used if no other configuration is specified */
new AvaATConfig('Default', array(
    'url' => 'no url specified',
    'addressService' => '/Address/AddressSvc.asmx',
    'taxService' => '/Tax/TaxSvc.asmx',
    'batchService' => '/Batch/BatchSvc.asmx',
    'avacert2Service' => '/AvaCert2/AvaCert2Svc.asmx',
    'addressWSDL' => 'file://' . $__wsdldir . '/Address.wsdl',
    'taxWSDL' => 'file://' . $__wsdldir . '/Tax.wsdl',
    'batchWSDL' => 'file://' . $__wsdldir . '/BatchSvc.wsdl',
    'avacert2WSDL' => 'file://' . $__wsdldir . '/AvaCert2Svc.wsdl',
    'account' => '<your account number here>',
    'license' => '<your license key here>',
    'adapter' => 'avatax4php,13.2.0.2',
    'client' => 'AvalaraPHPInterface,1.0',
    'name' => 'PHPAdapter',
    'trace' => true) // change to false for production
);