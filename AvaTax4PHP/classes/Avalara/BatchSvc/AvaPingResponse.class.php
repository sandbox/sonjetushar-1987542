<?php
/**
 * AvaPingResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaPingResponse {
        private $PingResult; // AvaPingResult

        public function setPingResult($value){$this->PingResult=$value;} // AvaPingResult
        public function getPingResult(){return $this->PingResult;} // AvaPingResult

    }

}