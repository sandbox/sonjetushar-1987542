<?php
/**
 * AvaBatchFetch.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFetch {
        private $FetchRequest; // FetchRequest

        public function setFetchRequest($value){$this->FetchRequest=$value;} // FetchRequest
        public function getFetchRequest(){return $this->FetchRequest;} // FetchRequest

    }

 }