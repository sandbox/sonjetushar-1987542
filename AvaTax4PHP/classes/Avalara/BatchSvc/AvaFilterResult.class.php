<?php
/**
 * AvaFilterResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaFilterResult {
        private $Count; // int

        public function setCount($value){$this->Count=$value;} // int
        public function getCount(){return $this->Count;} // int

    }

}