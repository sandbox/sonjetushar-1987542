<?php
/**
 * AvaBatchFileDelete.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFileDelete {
        private $DeleteRequest; // DeleteRequest

        public function setDeleteRequest($value){$this->DeleteRequest=$value;} // DeleteRequest
        public function getDeleteRequest(){return $this->DeleteRequest;} // DeleteRequest

    }

}