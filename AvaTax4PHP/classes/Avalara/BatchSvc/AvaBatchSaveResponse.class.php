<?php
/**
 * AvaBatchSaveResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchSaveResponse {
        private $BatchSaveResult; // BatchSaveResult

        public function setBatchSaveResult($value){$this->BatchSaveResult=$value;} // BatchSaveResult
        public function getBatchSaveResult(){return $this->BatchSaveResult;} // BatchSaveResult

    }

}