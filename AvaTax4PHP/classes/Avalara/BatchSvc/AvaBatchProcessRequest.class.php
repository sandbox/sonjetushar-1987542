<?php
/**
 * AvaBatchProcessRequest.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchProcessRequest //extends FilterRequest
    {
        private $Filters; // string
        private $MaxCount; // int

        function __construct()
        {
            $this->MaxCount=0;

        }

        public function setFilters($value){$this->Filters=$value;} // string
        public function getFilters(){return $this->Filters;} // string

        public function setMaxCount($value){$this->MaxCount=$value;} // int
        public function getMaxCount(){return $this->MaxCount;} // int

    }

}