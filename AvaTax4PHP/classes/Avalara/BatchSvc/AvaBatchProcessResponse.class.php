<?php
/**
 * AvaBatchProcessResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchProcessResponse {
        private $BatchProcessResult; // BatchProcessResult

        public function setBatchProcessResult($value){$this->BatchProcessResult=$value;} // BatchProcessResult
        public function getBatchProcessResult(){return $this->BatchProcessResult;} // BatchProcessResult

    }

}