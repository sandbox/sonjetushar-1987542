<?php
/**
 * AvaIsAuthorized.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaIsAuthorized {
        private $Operations; // string

        public function setOperations($value){$this->Operations=$value;} // string
        public function getOperations(){return $this->Operations;} // string

    }

}