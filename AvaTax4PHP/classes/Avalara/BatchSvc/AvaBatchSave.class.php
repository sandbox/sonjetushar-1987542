<?php
/**
 * AvaBatchSave.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchSave {
        private $Batch; // Batch

        public function setBatch($value){$this->Batch=$value;} // Batch
        public function getBatch(){return $this->Batch;} // Batch

    }

}