<?php
/**
 * AvaBatchFetchResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFetchResponse {
        private $BatchFetchResult; // BatchFetchResult

        public function setBatchFetchResult($value){$this->BatchFetchResult=$value;} // BatchFetchResult
        public function getBatchFetchResult(){return $this->BatchFetchResult;} // BatchFetchResult

    }

}