<?php
/**
 * AvaBatchFileFetchResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFileFetchResponse {
        private $BatchFileFetchResult; // BatchFileFetchResult

        public function setBatchFileFetchResult($value){$this->BatchFileFetchResult=$value;} // BatchFileFetchResult
        public function getBatchFileFetchResult(){return $this->BatchFileFetchResult;} // BatchFileFetchResult

    }

}
