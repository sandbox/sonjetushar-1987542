<?php
/**
 * AvaIsAuthorizedResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaIsAuthorizedResponse {
        private $IsAuthorizedResult; // AvaIsAuthorizedResult

        public function setIsAuthorizedResult($value){$this->IsAuthorizedResult=$value;} // IsAuthorizedResult
        public function getIsAuthorizedResult(){return $this->IsAuthorizedResult;} // IsAuthorizedResult

    }

}