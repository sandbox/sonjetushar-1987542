<?php
/**
 * AvaAdjustTaxResult.class.phpphp
 */

/**
 * Result data returned from {@link AvaTaxServiceSoap#getTax}.
 *
 * @see AdjustTaxRequest
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaAdjustTaxResult //extends AvaGetTaxResult
    {


        ///From AvaGetTaxResult- Bug in soapclient requires this copy
        private $DocCode;	//string
        private $AdjustmentDescription;      //string
        private $DocDate;			//date
        private $TaxDate;			//date
        private $DocType;			//AvaDocumentType
        private $DocStatus;			//DocStatus
        private $Reconciled;      //boolean
        private $Locked;			//boolean
        private $Timestamp;		//dateTime
        private $TotalAmount;		//decimal
        private $TotalDiscount;	//decimal
        private $TotalExemption;	//decimal
        private $TotalTaxable;	//decimal
        private $TotalTax;		//decimal
        private $HashCode;		//int
        private $AdjustmentReason;	//int
        private $Version;		//int
        private $TaxLines;	//ArrayOfTaxLine

        //@author:swetal
        //Added new properties to upgrade to 5.3 interface
        private $TotalTaxCalculated;	//decimal
        private $TaxSummary;		//ArrayOfTaxDetail



        /**
         * Gets the internal reference code used by the client application. This is used for operations such as Post and GetTaxHistory.
         * <p>
         * See {@link AvaGetTaxRequest#getDocCode} on <b>AvaGetTaxRequest</b> for more information about this member.
         * </p>
         *
         * @return string
         */
        public function getDocCode() {return $this->DocCode; }

        /**
         * AdjustmentDescription set while making AdjustTax call.
         *
         * @return string
         */
        public function getAdjustmentDescription() { return $this->AdjustmentDescription; }

        /**
         * AdjustmentReason set while making AdjustTax call. It is a high level classification of why an Original Document is being modified.
         *
         * @return int
         */
        public function getAdjustmentReason(){ return $this->AdjustmentReason; }

        /**
         * Gets the date on the invoice, purchase order, etc.
         * <p>
         * See {@link AvaGetTaxRequest#getDocDate} on <b>AvaGetTaxRequest</b> for more information about this member.
         * </p>
         *
         * @return date
         */
        public function getDocDate()  {return $this->DocDate; }

        /**
         * Tax Date is the date used to calculate tax on the Document.
         * <p>
         * See {@link AvaGetTaxRequest#taxDate} on <b>AvaGetTaxRequest</b> for more information about this member.
         * </p>
         *
         * @return date
         */
        public function getTaxDate()  {return $this->TaxDate; }

        /**
         * Gets the Document Type.
         * <p>
         * See {@link AvaGetTaxRequest#DocType} on <b>AvaGetTaxRequest</b> for more information about this member.
         * </p>
         *
         * @return AvaDocumentType
         */
        public function getDocType()  {return $this->DocType; }

        /**
         * Gets the document's status after the tax calculation.
         *
         * @return AvaDocStatus
         */
        public function getDocStatus()  {return $this->DocStatus; }

        /**
         * True if the document has been reconciled;  Only committed documents can be reconciled.
         * <p>
         * For information on committing documents, see the <b>TaxSvc</b>'s
         * {@link TaxSvcSoap#commitTax} method. For information
         * on reconciling documents, see the {@link TaxSvcSoap#reconcileTaxHistory} method.
         * </p>
         *
         * @return boolean
         */
        public function getIsReconciled()  {return $this->Reconciled; }

        /**
         * Flag indicating if a Document has been locked by Avalara's MRS process. Locked documents can not be modified and can not be cancelled because they have been reported on Tax Return.
         *
         * @return boolean
         */
        public function getLocked()  {return $this->Locked; }

        /**
         * Date of the last status change on the document (i.e. Save date, Post date, Commit date, Cancel date).
         *
         * @return dateTime
         */
        public function getTimestamp()  {return $this->Timestamp; }

        /**
         * The sum of all line {@link AvaLine#getAmount} values.
         *
         * @param decimal
         */
        public function getTotalAmount()  {return $this->TotalAmount; }

        /**
         * Gets the sum of all <b>AvaTaxLine</b> {@link AvaTaxLine#getDiscount} amounts; Typically it
         * will equal the requested Discount, but, but it is possible that no lines were marked as discounted.
         *
         * @return decimal
         */
        public function getTotalDiscount()  {return $this->TotalDiscount; }

        /**
         * Gets the sum of all <b>AvaTaxLine</b> {@link AvaTaxLine#getExemption} amounts.
         *
         * @return decimal
         * @deprecated See {@link AvaTaxDetail#getExemption}.
         */
        public function getTotalExemption()  {return $this->TotalExemption; }

        /**
         * Gets the amount the tax is based on;  This is the total of all {@link AvaLine} <b>BaseSvc</b> amounts;
         * Typically it will be equal to the document
         * {@link AvaGetTaxResult#getTotalAmount} - {@link AvaGetTaxRequest#getDiscount} - {@link #getTotalExemption}.
         *
         * @return decimal
         * @deprecated See {@link AvaTaxDetail#getTaxable}.
         */
        public function getTotalTaxable()  {return $this->TotalTaxable; }

        /**
         *  Gets the total tax for the document.
         *
         * @return decimal
         */
        public function getTotalTax()  {return $this->TotalTax; }

        /**
         * HashCode to support Reconciliation.
         *
         * @return hashCode
         */
        public function getHashCode()  {return $this->HashCode; }

        /**
         * Current version of the document.
         *
         * @return int
         */
        public function getVersion()  {return $this->Version; }

        /**
         * Gets the Tax broken down by individual {@link AvaTaxLine}.
         *
         * @return taxLines
         */
        public function getTaxLines()  {return EnsureIsArray($this->TaxLines->TaxLine); }

        /**
         * TotalTaxCalculated indicates the total tax calculated by AvaTax. This is usually the same as the TotalTax, except when a tax override amount is specified.
         * This is for informational purposes.The TotalTax will still be used for reporting
         *
         * @return unknown
         */
        public function getTotalTaxCalculated(){ return $this->TotalTaxCalculated;}

        /**
         * TaxSummary is now returned when AvaGetTaxRequest.AvaDetailLevel == AvaDetailLevel.AvaLine in addition to AvaDetailLevel.Summary.
         * It is not returned for AvaDetailLevel.Document or AvaDetailLevel.AvaTaxDetail.
         *
         * @return ArrayOfTaxDetail
         */
        public function getTaxSummary()	{return EnsureIsArray($this->TaxSummary->TaxDetail);}

        public function getTaxLine($lineNo)
        {
            if($this->getTaxLines() != null)
            {
                foreach($this->getTaxLines() as $taxLine)
                {
                    if($lineNo == $taxLine->getNo())
                    {
                        return $taxLine;
                    }

                }
            }
        }


        /////////////////////////////////////////////PHP bug requires this copy from AvaBaseResult ///////////
        /**
         * @var string
         */
        private $TransactionId;
        /**
         * @var string must be one of the values defined in {@link AvaSeverityLevel}.
         */
        private $ResultCode = 'Success';
        /**
         * @var array of Message.
         */
        private $Messages = array();

        /**
         * Accessor
         * @return string
         */
        public function getTransactionId() { return $this->TransactionId; }
        /**
         * Accessor
         * @return string
         */
        public function getResultCode() { return $this->ResultCode; }
        /**
         * Accessor
         * @return array
         */
        public function getMessages() { return EnsureIsArray($this->Messages->Message); }




    }

}