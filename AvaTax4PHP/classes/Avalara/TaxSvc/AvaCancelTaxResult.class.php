<?php
/**
 * AvaCancelTaxResult.class.phpphp
 */

/**
 * Result data returned from {@link TaxSvcSoap#cancelTax}
 * @see CancelTaxRequest
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 *
 */
namespace Avalara\TaxSvc {
    class AvaCancelTaxResult // extends AvaBaseResult
    {


// AvaBaseResult innards - workaround a bug in SoapClient

        /**
         * @var string
         */
        private $TransactionId;
        /**
         * @var string must be one of the values defined in {@link AvaSeverityLevel}.
         */
        private $ResultCode = 'Success';
        /**
         * @var array of Message.
         */
        private $Messages = array();

        /**
         * Accessor
         * @return string
         */
        public function getTransactionId() { return $this->TransactionId; }
        /**
         * Accessor
         * @return string
         */
        public function getResultCode() { return $this->ResultCode; }
        /**
         * Accessor
         * @return array
         */
        public function getMessages() { return EnsureIsArray($this->Messages->Message); }



    }

}