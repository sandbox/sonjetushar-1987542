<?php
/**
 * AvaTaxOverrideide.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    use DateTime as DateTime;
    class AvaTaxOverride
    {
        private $TaxOverrideType;   //AvaTaxOverrideType
        private $TaxAmount;         //decimal
        private $TaxDate;           //date
        private $Reason;            //string



        public function __construct()
        {
            $this->TaxAmount=0.0;

            $dateTime=new DateTime();
            $dateTime->setDate(1900,01,01);

            $this->TaxDate=$dateTime->format("Y-m-d");
        }

        public function setTaxOverrideType($value){ $this->TaxOverrideType=$value; }   //AvaTaxOverrideType
        public function setTaxAmount($value){$this->TaxAmount=$value;}         //decimal
        public function setTaxDate($value){$this->TaxDate=$value;}           //date
        public function setReason($value){$this->Reason=$value;}            //string


        public function getTaxOverrideType(){ return $this->TaxOverrideType; }   //AvaTaxOverrideType
        public function getTaxAmount(){return $this->TaxAmount;}         //decimal
        public function getTaxDate(){return $this->TaxDate;}           //date
        public function getReason(){return $this->Reason;}            //string


    }

}