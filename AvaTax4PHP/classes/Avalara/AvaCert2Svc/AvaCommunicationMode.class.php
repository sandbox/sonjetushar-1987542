<?php
/**
 * AvaCommunicationMode.class.php
 */

/**
 * AvaCommunicationMode indicates the mode to use for communicating with the customer.
 * @see AvaCertificateRequestInitiateRequest
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    class AvaCommunicationMode {

        /**
         * The value has not been set.
         */
        public static $NULL = 'NULL';

        /**
         * Email address
         */
        public static $EMAIL = 'EMAIL';

        /**
         * Mail address
         */
        public static $MAIL = 'MAIL';

        /**
         * Fax number
         */
        public static $FAX = 'FAX';

        public static function Values()
        {
            return array(
                AvaCommunicationMode::$NULL,
                AvaCommunicationMode::$EMAIL,
                AvaCommunicationMode::$MAIL,
                AvaCommunicationMode::$FAX

            );
        }

    }

}