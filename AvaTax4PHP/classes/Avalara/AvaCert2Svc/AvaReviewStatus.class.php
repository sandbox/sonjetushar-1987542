<?php
/**
 * AvaReviewStatus.class.php
 */

/**
 * AvaReviewStatus indicates the review status for the Certificate record.
 * @see ExemptionCertificate
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaReviewStatus extends AvaEnum
    {
        /**
         *  The certificate has not yet been reviewed.
         *
         * @var AvaReviewStatus
         */
        public static $PENDING	= 'PENDING';

        /**
         *  The certificate was accepted during review.
         *
         * @var AvaReviewStatus
         */
        public static $ACCEPTED	= 'ACCEPTED';

        /**
         *  The certificate was rejected during review.
         *
         * @var AvaReviewStatus
         */
        public static $REJECTED	= 'REJECTED';

        public static function Values()
        {
            return array(
                AvaReviewStatus::$PENDING,
                AvaReviewStatus::$ACCEPTED,
                AvaReviewStatus::$REJECTED
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

    }

}