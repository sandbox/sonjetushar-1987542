<?php
/**
 * AvaCertificateStatus.class.php
 */

/**
 * AvaCertificateStatus indicates the status for the Certificate record.
 * @see ExemptionCertificate
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaCertificateStatus extends AvaEnum
    {
        /**
         *  The certificate is active with images received.
         *
         * @var AvaCertificateStatus
         */
        public static $ACTIVE	= 'ACTIVE';

        /**
         *  The certificate has been voided from active use.
         *
         * @var AvaCertificateStatus
         */
        public static $VOID	= 'VOID';

        /**
         *  The certificate does not yet have all of its images received.
         *
         * @var AvaCertificateStatus
         */
        public static $INCOMPLETE	= 'INCOMPLETE';

        public static function Values()
        {
            return array(
                AvaCertificateStatus::$ACTIVE,
                AvaCertificateStatus::$VOID,
                AvaCertificateStatus::$INCOMPLETE
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

    }

}