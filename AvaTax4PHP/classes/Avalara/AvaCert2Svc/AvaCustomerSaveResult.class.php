<?php
/**
 * AvaCustomerSaveResult.class.php
 */

/**
 * Contains the customer save operation result returned by {@link CustomerSave}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaCustomerSaveResult extends AvaBaseResult {

    }

 }